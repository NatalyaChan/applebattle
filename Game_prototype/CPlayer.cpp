#include "CPlayer.h"



CPlayer::CPlayer()
{
}



CPlayer::CPlayer(std::string newname, std::string sizeCategory, int speed, std::vector<SDL_Point> newSpriteDimentions)
{
	setMainAttributes(newname, speed, newSpriteDimentions);
	setsizeCategory(sizeCategory);
}



CPlayer::~CPlayer()
{
	clear();
}



void CPlayer::setMainAttributes(std::string newname, int speed, std::vector<SDL_Point> newSpriteDimentions)
{
	setName(newname);
	setSpeed(speed);
	setSpriteDimentions(newSpriteDimentions);
}



void CPlayer::setName(std::string newname)
{
	name = newname;
}



std::string CPlayer::getName()
{
	return name;
}



void CPlayer::setPosition(int x, int y)
{
	playerPosition = { x, y };
}



int CPlayer::getX()
{
	return playerPosition.x;
}



int CPlayer::getY()
{
	return playerPosition.y;
}



int  CPlayer::getFloorY()
{
	return getY() + getHeight();
}



int CPlayer::getWidth()
{
	return currentClip->w;
}



int CPlayer::getHeight()
{
	return currentClip->h;
}



void CPlayer::setsizeCategory(std::string sizeCategory)
{
	this->sizeCategory = sizeCategory;
}



std::string CPlayer::getsizeCategory()
{
	return sizeCategory;
}



void CPlayer::setSpeed(int speed)
{
	this->speed = speed;
}



int CPlayer::getSpeed()
{
	return speed;
}



int CPlayer::getScore()
{
	return score;
}



void CPlayer::clearScore()
{
	score = 0;
}



void CPlayer::setSpriteDimentions(std::vector<SDL_Point> newSpriteDimentions)
{
	for (int i = 0; i < newSpriteDimentions.size(); i++)
	{
		spriteDimentions.at(i) = newSpriteDimentions[i];
	}
}



std::vector<SDL_Point> CPlayer::getSpriteDimentions()
{
	return spriteDimentions;
}



void CPlayer::moveUp()
{
	currentClip = &spriteUpClips[currentFrame / nFrames];
	playerPosition.y = playerPosition.y - speed;
}



void CPlayer::moveDown()
{
	currentClip = &spriteDownClips[currentFrame / nFrames];
	playerPosition.y = playerPosition.y + speed;
}



void CPlayer::moveLeft()
{
	currentClip = &spriteLeftClips[currentFrame / nFrames];
	playerPosition.x = playerPosition.x - speed;
}



void CPlayer::moveRight()
{
	currentClip = &spriteRightClips[currentFrame / nFrames];
	playerPosition.x = playerPosition.x + speed;
}



bool CPlayer::isFruitUp(CThing* t)
{
	return (t->getCenterY() < playerPosition.y);
}



bool CPlayer::isFruitDown(CThing* t)
{
	return (playerPosition.y + getHeight() < t->getCenterY());
}



bool CPlayer::isFruitLeft(CThing* t)
{
	return (t->getCenterX() < playerPosition.x);
}



bool CPlayer::isFruitRight(CThing* t)
{
	return (playerPosition.x + getWidth() < t->getCenterX());
}



void CPlayer::CheckFruit(CThing* t, int screenwidth, int screenheight)
{
	//checking if the player is touching the fruit
	if (!isFruitUp(t) && !isFruitDown(t) && !isFruitLeft(t) && !isFruitRight(t))
	{
		score += 1;

		//add new fruit
		t->setX(rand() % (screenwidth - t->getWidth()));
		t->setY(rand() % (screenheight - t->getHeight()));
	}
}



bool CPlayer::loadMedia(SDL_Renderer* ren, TTF_Font* font)
{
	bool success = true;

	//loading player texture
	if (!spriteSheetTexture->loadFromFile(ren, "Images/" + getName() + "Sheet.png"))
	{
		printf("Failed to load walking animation texture!\n");
		success = false;
	}
	else
	{
		//Set sprite clips 
		for (int i = 0; i < nFrames; i++)
		{
			spriteLeftClips.push_back({ i * spriteDimentions[0].x,0,spriteDimentions[0].x,spriteDimentions[0].y });
			spriteRightClips.push_back({ i * spriteDimentions[1].x,spriteDimentions[0].y,spriteDimentions[1].x,spriteDimentions[1].y });
			spriteUpClips.push_back({ i * spriteDimentions[2].x,spriteDimentions[0].y + spriteDimentions[1].y,spriteDimentions[2].x,spriteDimentions[2].y });
			spriteDownClips.push_back({ i * spriteDimentions[3].x,spriteDimentions[0].y + spriteDimentions[1].y + spriteDimentions[2].y,spriteDimentions[3].x,spriteDimentions[3].y });
		}
		spriteIdleClip = { 4 * spriteDimentions[2].x,spriteDimentions[0].y + spriteDimentions[1].y,spriteDimentions[4].x,spriteDimentions[4].y };

		//removing extra memory
		spriteLeftClips.shrink_to_fit();
		spriteRightClips.shrink_to_fit();
		spriteUpClips.shrink_to_fit();
		spriteDownClips.shrink_to_fit();

		currentClip = &spriteIdleClip;

	}


	SDL_Color textColor = { 0, 0, 0, 255 }; //Set text color as black 

	//Load prompt texture for score
	if (!promptScoreTexture->loadFromRenderedText(ren, name + "'s Score: ", font, textColor))
	{
		printf("Unable to render prompt texture!\n");
		success = false;
	}

	return success;
}



void CPlayer::PlayerLogic(CThing* t, int screenwidth, int screenheight)
{
	//Set texture based on current keystate 
	const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);
	if (currentKeyStates[SDL_SCANCODE_UP])
	{
		moveUp();
		if (playerPosition.y < speed / 2 - getHeight())
		{
			playerPosition.y = screenheight - speed / 2;
		}
	}
	else if (currentKeyStates[SDL_SCANCODE_DOWN])
	{
		moveDown();
		if (playerPosition.y > screenheight)
		{
			playerPosition.y = speed / 2 - getHeight();
		}
	}
	else if (currentKeyStates[SDL_SCANCODE_LEFT])
	{
		moveLeft();
		if (playerPosition.x < speed / 2 - getWidth())
		{
			playerPosition.x = screenwidth - speed / 2;
		}
	}
	else if (currentKeyStates[SDL_SCANCODE_RIGHT])
	{
		moveRight();
		if (playerPosition.x > screenwidth)
		{
			playerPosition.x = speed / 2 - getWidth();
		}
	}
	else
	{
		currentClip = &spriteIdleClip;
	}

	CheckFruit(t, screenwidth, screenheight);  //checking if the player is touching the fruit
}



void CPlayer::writeScore(SDL_Renderer* ren, int x, int y, TTF_Font* font)
{
	SDL_Color textColor = { 0, 0, 0 }; //Set text color as black 

	//Render text 
	if (!scoreTexture->loadFromRenderedText(ren, std::to_string(score), font, textColor))
	{
		printf("Unable to render score texture!\n");
	}
	else
	{
		promptScoreTexture->render(ren, x, y);
		scoreTexture->render(ren, x + promptScoreTexture->getWidth(), y);
	}
}



void CPlayer::DrawPlayer(SDL_Renderer* ren, int x, int y)
{
	spriteSheetTexture->render(ren, x, y, currentClip);  //Render current frame 

	++currentFrame;  //Go to next frame 

	//Cycle animation 
	if (currentFrame / nFrames >= nFrames)
	{
		currentFrame = 0;
	}
}



void CPlayer::clear()
{
	setPosition(0, 0);
	setSpeed(3);
	clearScore();
	spriteDimentions = { {0,0},{0,0},{0,0},{0,0},{0,0} };
	currentClip = NULL;
	spriteLeftClips.clear();
	spriteRightClips.clear();
	spriteUpClips.clear();
	spriteDownClips.clear();
	spriteSheetTexture->free();
	promptScoreTexture->free();
	scoreTexture->free();
}