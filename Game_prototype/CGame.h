#include <SDL.h>
#include <stdio.h>
#include <SDL_image.h>
#include <string>
#include "CRival.h"
#include "CThing.h"
#include "CTexture.h"
#include "CButton.h"
#include <vector>
#include <ctime>
#include <memory>



#ifndef CGAME_H
#define	CGAME_H



class CGame {
public:
	CGame();
	~CGame();   //Free resources and close SDL
	bool Init();   //Starts up SDL and creates window 
	bool loadMedia();  //Loads media

	void quitCheck(SDL_Event& e);

	//loops
	void titleScreenLoop();
	bool characterSelectorScreenLoop();
	bool levelScreenLoop();
	bool finishScreenLoop();
	//bool captionScreenLoop();
	void mainLoop();

	void mainLogic();
	void titleAppears();
	void draw(CPlayer* p1 = NULL, CPlayer* p2 = NULL, CThing* thing = NULL, CButton* button = NULL, CTexture* text = NULL);

private:
	SDL_Window* gWindow = NULL; //The window we'll be rendering to 
	SDL_Renderer* gRenderer = NULL;  //The window renderer 

	//screen size
	const int SCREEN_WIDTH = 800;
	const int SCREEN_HEIGHT = 600;

	//Globally used fonts
	TTF_Font* mainFont = NULL;
	TTF_Font* subFont = NULL;

	//creates players
	std::unique_ptr<CPlayer> player1 = std::make_unique<CPlayer>();
	std::unique_ptr<CPlayer> player2 = std::make_unique<CRival>();

	std::unique_ptr <CTexture> backGroundSheetTexture = std::make_unique<CTexture>();
	std::unique_ptr <CTexture> buttonSpriteSheetTexture = std::make_unique<CTexture>();
	std::unique_ptr <CTexture> titleTexture = std::make_unique<CTexture>();
	std::vector<SDL_Rect> backGroundSceneClips;
	SDL_Rect* currentBackGroundClip;

	int titleFrame;

	bool quit = false; //Main loop flag  
	bool pause = true;  //pause flag

	std::string winner;  //
	int victoryCondition = 20;

	enum scenes {
		titleScreen = 0,
		selectorScreen = 1,
		levelScreen = 2,
		finishScreen = 3,
		//	captionScreen = 4,
		total = 4
	};
	int currentScene;
};


#endif	//CGAME_H