#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include "CTexture.h"

#ifndef CTHING_H
#define	CTHING_H

class CThing
{
public:
	CThing();
	CThing(std::string newname);
	~CThing();

	std::string getName();

	void setPosition(int x, int y);
	void setX(int x);
	void setY(int y);
	int getX();
	int getY();
	int getCenterX();
	int getCenterY();

	int getWidth();
	int getHeight();

	bool loadMedia(SDL_Renderer* ren);
	void drawThing(SDL_Renderer* ren);

private:
	std::string name = "Default";
	SDL_Point thingPosition = { 0,0 };
	std::unique_ptr <CTexture> thingSprite = std::make_unique<CTexture>();
};

#endif //CTHING_H