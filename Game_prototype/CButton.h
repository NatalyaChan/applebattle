#include "CTexture.h"
#include "CPlayer.h"


#ifndef CBUTTON_H
#define	CBUTTON_H

class CButton {
public:
	//Initializes internal variables 
	CButton();
	CButton(int x, int y, int w, int h, int spriteX = 0, int spriteY = 0, int spriteW = 0, int spriteH = 0);
	~CButton();

	//Sets top left position 
	void setPosition(int x, int y);
	int getX();
	int getY();

	void setSize(int w, int h);
	int getWidth();
	int getHeight();

	void setButtonSpriteClip(int x, int y, int w, int h);

	//Handles mouse event 
	bool handleEvent(SDL_Event* e);

	void addTextOnButton(SDL_Renderer* ren, std::string title, TTF_Font* font, SDL_Color textColor, int align = 1);  // align = 0=top; 1=middle; 2=bottom

	//Shows button sprite 
	void DrawButton(SDL_Renderer* ren, CTexture* spriteSheet = NULL);

private:
	SDL_Point buttonPosition = { 0,0 };  //Top left position 
	SDL_Point buttonSize = { 120, 60 };   //    Size of window
	SDL_Point buttonTextPosition = { 0,0 };

	//button sprite
	SDL_Rect spriteButtonClip = { 0,0,120,60 };
	std::unique_ptr <CTexture> buttonText = std::make_unique<CTexture>();
};


#endif	//CBUTTON_H