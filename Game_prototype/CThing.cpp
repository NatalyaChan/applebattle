#include "CThing.h"



CThing::CThing()
{
}



CThing::CThing(std::string newname)
{
	this->name = newname;
}



CThing::~CThing()
{
	thingSprite->free();
}



std::string CThing::getName()
{
	return name;
}



void CThing::setPosition(int x, int y)
{
	thingPosition = { x,y };
}



void CThing::setX(int x)
{
	thingPosition.x = x;
}



void CThing::setY(int y)
{
	thingPosition.y = y;
}



int CThing::getX()
{
	return thingPosition.x;
}



int CThing::getY()
{
	return thingPosition.y;
}



int CThing::getCenterX()
{
	return thingPosition.x + getWidth() / 2;
}



int CThing::getCenterY()
{
	return thingPosition.y + getHeight() / 2;
}



int CThing::getWidth()
{
	return thingSprite->getWidth();
}



int CThing::getHeight()
{
	return thingSprite->getHeight();
}



bool CThing::loadMedia(SDL_Renderer* ren)
{
	bool success = true;

	if (!thingSprite->loadFromFile(ren, "Images/" + getName() + ".png"))
	{
		printf("Failed to load thing image!\n");
		success = false;
	}
	return success;
}



void CThing::drawThing(SDL_Renderer* ren)
{
	//Render texture to screen 
	thingSprite->render(ren, getX(), getY());
}