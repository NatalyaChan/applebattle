#include "LTexture.h"
#include "Player.h"

#ifndef LBUTTON_H
#define	LBUTTON_H

class LButton {
public:
	//Initializes internal variables 
	LButton();
	~LButton();

	//Sets top left position 
	void setPosition(int x, int y);
	int getX();
	int getY();

	void setSize(int w, int h);
	int getWidth();
	int getHeight();

	void setButtonSpriteClip(int x, int y, int w, int h);

	//Handles mouse event 
	bool handleEvent(SDL_Event* e);

	void addTextOnButton(SDL_Renderer* ren, std::string title, TTF_Font* font, SDL_Color textColor, int align=1);  // align =0= top; 1=middle; 2=bottom

	//Shows button sprite 
	void DrawButton(SDL_Renderer* ren, LTexture* spriteSheet);

private:
	SDL_Point buttonPosition;  //Top left position 
	SDL_Point buttonSize;   //    Size of window
	SDL_Point buttonTextPosition;

	//button sprite
	SDL_Rect spriteButtonClip;
	LTexture buttonText;
	LTexture* prtButtonText;
};


#endif	//LBUTTON_H