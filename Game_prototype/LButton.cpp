#include "LButton.h"



LButton::LButton() 
{ 
	setPosition ( 0,0 ) ;
	setSize(120, 60);
	setButtonSpriteClip( 0,0,120,60);

	prtButtonText = NULL;
	//currentClip = &SpriteButtonClips[0];
	printf("Button created\n");
} 



LButton::~LButton()
{

	buttonText.free();
	prtButtonText = NULL;
	//buttonSpriteSheetTexture.free();
	printf("deletebutton\n");
}



void LButton::setPosition(int x, int y) 
{ 
	buttonPosition = { x,y };
}



int LButton::getX()
{
	return buttonPosition.x;
}



int LButton::getY()
{
	return buttonPosition.y;
}



void LButton::setSize(int w, int h)
{
	buttonSize = { w, h };
}



int LButton::getWidth()
{
	return buttonSize.x;
}



int LButton::getHeight()
{
	return buttonSize.y;
}



void LButton::setButtonSpriteClip(int x, int y, int w, int h)
{
	spriteButtonClip = {x,y,w,h};
}



bool LButton::handleEvent(SDL_Event* e) 
{ 
	bool press = false;
	//If mouse event happened 
	if(e->type == SDL_MOUSEBUTTONDOWN) 
	{ 
		//Get mouse position 
		int x, y; 
		SDL_GetMouseState( &x, &y );
		
		//Mouse is left of the button 
		if(( x >= buttonPosition.x ) &&
			(x <= buttonPosition.x + buttonSize.x) &&
			(y >= buttonPosition.y) && 
			(y <= buttonPosition.y + buttonSize.y))
		{ 
			//currentClip = &SpriteButtonClips[2];
			press = true;
		} 
		else 
		{ 
		//	currentClip = &SpriteButtonClips[0];
		} 
	} 
	return press;
}



//bool LButton::loadMedia(SDL_Renderer* ren)
//{
//	bool success = true;
//
//
//	if (!buttonSpriteSheetTexture.loadFromFile(ren, "Images/button.png"))
//	{
//		printf("Failed to load button texture!\n");
//		success = false;
//	}
//	else
//	{
//		for (int i = 0; i < 4; ++i)
//		{
//			SpriteButtonClips[i] = { 0,i * buttonSize.y,buttonSize.x,buttonSize.y };
//		}
//	}
//
//	return success;
//}



//void LButton::createButton(SDL_Renderer* ren, int x, int y, TTF_Font* font, std::string title, Player* player)
//{
//	buttonPosition = { x,y};
//	//Render red filled quad 
//	SDL_Rect fillRect = { buttonPosition.x, buttonPosition.y, buttonSize.x, buttonSize.y };
//	SDL_SetRenderDrawColor( ren, 0xFF, 0xFF, 0xFF, 0xFF ); 
//	SDL_RenderFillRect( ren, &fillRect );
//	
//	if (player != NULL)
//	{
//		
//	}
//	if (!title.empty())
//	{
//		LTexture titleTexture = LTexture();
//		SDL_Color textColor = { 0, 0, 0 };
//		titleTexture.loadFromRenderedText(ren, title, font, textColor);
//		titleTexture.render(ren, (buttonPosition.x + (buttonSize.x - buttonText.getWidth()) / 2), (buttonPosition.y + buttonSize.y - titleTexture.getHeight() - 10));
//	}
//
//}



void LButton::addTextOnButton(SDL_Renderer* ren, std::string title, TTF_Font* font, SDL_Color textColor, int align)   // align =0= top; 1=middle; 2=bottom
{
	if (!buttonText.loadFromRenderedText(ren, title, font, textColor))
	{
		printf("Unable to render buttonText texture!\n");
	}
	else
	{
		buttonTextPosition = { (buttonPosition.x + (buttonSize.x - buttonText.getWidth()) / 2),
	                        (buttonPosition.y + (buttonSize.y - buttonText.getHeight()-20)* align/2 + 10) };
	}
	prtButtonText = &buttonText;
	buttonText.render(ren, buttonTextPosition.x, buttonTextPosition.y);
}




void LButton::DrawButton(SDL_Renderer* ren, LTexture* spriteSheet)
{
	//Show current button sprite 
	spriteSheet->render(ren, buttonPosition.x, buttonPosition.y, &spriteButtonClip);
	if (prtButtonText != NULL)
	{
		prtButtonText->render(ren, buttonTextPosition.x, buttonTextPosition.y);
	}
	
	
}