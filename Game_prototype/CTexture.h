#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <string>
#include <memory>

#ifndef CTEXTURE_H
#define	CTEXTURE_H

//Texture wrapper class 
class CTexture
{
public:
	CTexture(); //Initializes variables 
	~CTexture(); //Deallocates memory 

	//Gets image dimensions 
	int getWidth();
	int getHeight();

	bool loadFromFile(SDL_Renderer* ren, std::string path);  //Loads image at specified path 
	bool loadFromRenderedText(SDL_Renderer* ren, std::string textureText, TTF_Font* font, SDL_Color textColor); //Creates image from font string 

	//Renders texture at given point 
	void render(SDL_Renderer* ren, int x, int y, SDL_Rect* clip = NULL);
	void render(SDL_Renderer* ren, int x, int y, int w, int h, SDL_Rect* clip = NULL);

	void free(); //Deallocates texture 

private:
	SDL_Texture* mTexture;  //The actual hardware texture 
	SDL_Point textureSize;  //Image dimensions
};


#endif	//CTEXTURE_H