#include "CGame.h"


int main(int argc, char* args[])
{
	std::unique_ptr<CGame> game = std::make_unique<CGame>();

	if (!game->Init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Load media 
		if (!game->loadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{
			game->mainLoop();
		}
	}

	return 0;
}




