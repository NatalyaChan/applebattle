#include "CPlayer.h"

#ifndef CRIVAL_H
#define	CRIVAL_H

class CRival :
	public CPlayer
{
public:
	CRival();
	~CRival();

	void PlayerLogic(CThing* t, int screenwidth, int screenheight); //checks where the fruit is and goes to it

private:
};

#endif	//RIVAL_H