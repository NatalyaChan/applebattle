#include "CGame.h"


CGame::CGame()
{
	currentScene = 0;
	titleFrame = 0;
}



CGame::~CGame()
{
	//Free loaded images 
	backGroundSheetTexture->free();
	buttonSpriteSheetTexture->free();
	titleTexture->free();
	currentBackGroundClip = NULL;

	//Free global font 
	TTF_CloseFont(mainFont);
	mainFont = NULL;
	TTF_CloseFont(subFont);
	subFont = NULL;

	//Destroy window 
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}



bool CGame::Init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Create window
		gWindow = SDL_CreateWindow("Apple Battle", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create renderer for window 
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer color 
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading 
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}

				//Initialize SDL_ttf 
				if (TTF_Init() == -1)
				{
					printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}



bool CGame::loadMedia()
{
	bool success = true;  //Loading success flag 

	//Load background texture
	if (!backGroundSheetTexture->loadFromFile(gRenderer, "Images/backgroundSheet.png"))
	{
		printf("Failed to load BackGroundTexture image!\n");
		success = false;
	}
	else
	{
		for (int i = 0; i < finishScreen; i++)
		{
			backGroundSceneClips.push_back({ 0,i * SCREEN_HEIGHT,SCREEN_WIDTH,SCREEN_HEIGHT });
		}
		backGroundSceneClips.shrink_to_fit();
	}

	//Load texture for buttons
	if (!buttonSpriteSheetTexture->loadFromFile(gRenderer, "Images/buttonsSheet.png"))
	{
		printf("Failed to load button texture!\n");
		success = false;
	}

	if (!titleTexture->loadFromFile(gRenderer, "Images/title.png"))
	{
		printf("Failed to load title texture!\n");
		success = false;
	}



	//Open the fonts
	mainFont = TTF_OpenFont("Fonts/tahomabd.ttf", 28);
	if (mainFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}

	subFont = TTF_OpenFont("Fonts/arial.ttf", 18);
	if (mainFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}

	return success;
}



void CGame::quitCheck(SDL_Event& e)
{
	while (SDL_PollEvent(&e) != 0)
	{
		//user request quit
		if (e.type == SDL_QUIT)
		{
			quit = true;
		}

		//user request pause
		else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN)
		{
			pause = !pause;
		}
	};
}



void CGame::titleScreenLoop()
{
	SDL_Event event;

	//creating start button
	std::unique_ptr <CButton> startButton(new CButton(280, 400, 264, 120, 0, 0, 264, 120));

	currentBackGroundClip = &backGroundSceneClips[titleScreen];

	while ((!quit) && (currentScene == titleScreen)) //While title screen is running 
	{
		quitCheck(event);

		if (startButton->handleEvent(&event) || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN))
		{
			currentScene += 1; //go to next screen
		};

		draw(NULL, NULL, NULL, startButton.get());
	};
}



bool CGame::characterSelectorScreenLoop()
{
	currentBackGroundClip = &backGroundSceneClips[selectorScreen];
	draw();  //background drawing

	bool success = true;

	SDL_Event event;
	int selectedPlayer = 0;
	int randomRival = 0;

	//creating array of possible players
	std::vector <std::unique_ptr<CPlayer>> potentialPlayers;
	potentialPlayers.push_back(std::unique_ptr<CPlayer>(new CPlayer("Nya", "Little", 3, { {77,61},{77,61},{41,52},{41,58},{62,55} })));
	potentialPlayers.push_back(std::unique_ptr<CPlayer>(new CPlayer("Mighty", "Big", 2, { {113,165},{112,165},{94,175},{94,174},{90,174} })));
	potentialPlayers.push_back(std::unique_ptr<CPlayer>(new CPlayer("Neokortik", "Big", 2, { {102,162},{101,162 },{92,171},{92,171},{88,159} })));
	potentialPlayers.push_back(std::unique_ptr<CPlayer>(new CPlayer("Darky", "Little", 3, { {50,54},{50,54},{54,54},{54,55},{42,55} })));
	//potentialPlayers.push_back(std::unique_ptr<CPlayer>(new CPlayer("Pyika", "Medium", 3, { {83,95},{83,95},{83,95},{83,95},{83,95} })));

	//loading textures of possible players
	for (int i = 0; i < potentialPlayers.size(); i++)
	{
		if (!potentialPlayers[i]->loadMedia(gRenderer, mainFont))
		{
			printf("Failed to load player's image!\n");
			success = false;
		};
	}

	//if all textures are loaded successfully
	if (success)
	{
		SDL_Color textColor = { 0, 0, 0, 255 };

		//creating variables for drawing player parameters
		std::unique_ptr <CTexture> speedLabel = std::make_unique<CTexture>();
		std::unique_ptr <CTexture> sizeLabel = std::make_unique<CTexture>();

		//creating and drawing an array of buttons
		std::vector < std::unique_ptr<CButton>> buttonplayers(potentialPlayers.size());
		int buttonsInRow = 3; //tabulation, number of buttons in the same row. 4 also looks good

		//temporary variables
		int buttonX = 0;
		int buttonY = 0;
		int buttonW = 150;
		int buttonH = 200;
		int playerX = 0;
		int playerY = 0;

		for (int i = 0; i < potentialPlayers.size(); i++)
		{
			buttonX = static_cast<int>((i % buttonsInRow + 1) * SCREEN_WIDTH / (buttonsInRow + 1) - buttonW / floor((buttonsInRow + 1) / 2));
			buttonY = static_cast<int>((i / buttonsInRow) * SCREEN_HEIGHT / 2 + 30);
			buttonplayers[i].reset(new CButton(buttonX, buttonY, buttonW, buttonH));
			buttonplayers[i]->addTextOnButton(gRenderer, potentialPlayers[i]->getName(), mainFont, textColor, 2);
			buttonplayers[i]->DrawButton(gRenderer);

			playerX = static_cast<int>(buttonX + buttonW / 2 - potentialPlayers[i]->getWidth() / 2);
			playerY = static_cast<int>(buttonY + buttonH / 2 - potentialPlayers[i]->getHeight() / 2);
			potentialPlayers[i]->DrawPlayer(gRenderer, playerX, playerY);

			//drawing player parameters (speed, size)
			if (!speedLabel->loadFromRenderedText(gRenderer, "Speed: " + std::to_string(potentialPlayers[i]->getSpeed()), subFont, textColor))
			{
				printf("Unable to render speed texture!\n");
			}
			else
			{
				speedLabel->render(gRenderer, buttonX, buttonY + buttonH + 5);
			}

			if (!sizeLabel->loadFromRenderedText(gRenderer, "Size: " + potentialPlayers[i]->getsizeCategory(), subFont, textColor))
			{
				printf("Unable to render size texture!\n");
			}
			else
			{
				sizeLabel->render(gRenderer, buttonX, buttonY + buttonH + speedLabel->getHeight() + 10);
			}
		}

		//add inscription showing the number of apples required to win
		std::unique_ptr <CTexture> victoryConditionTexture = std::make_unique<CTexture>();
		if (!victoryConditionTexture->loadFromRenderedText(gRenderer, std::to_string(victoryCondition), mainFont, { 107,00,00 }))
		{
			printf("Unable to render victoryConditionTexture texture!\n");
		}
		else
		{
			victoryConditionTexture->render(gRenderer, 620, 490);
		}

		//Update screen
		SDL_RenderPresent(gRenderer);

		while ((!quit) && (currentScene == selectorScreen)) //While selector screen is running 
		{
			quitCheck(event);

			//checks if player has pressed button
			for (int i = 0; i < buttonplayers.size(); i++)
			{
				if (buttonplayers[i]->handleEvent(&event))
				{
					currentScene += 1;  //go to next screen
					player1 = move(potentialPlayers[i]);  //saves selected player
					selectedPlayer = i;
				};
			}
		}

		srand(static_cast<int>(time(0))); //randomize

		do
		{
			randomRival = rand() % potentialPlayers.size();   //selects random opponent
		} while (randomRival == selectedPlayer);  //checks whether it is equal to the selected player

		//saves selected rival
		player2->setMainAttributes(potentialPlayers[randomRival]->getName(), potentialPlayers[randomRival]->getSpeed(), potentialPlayers[randomRival]->getSpriteDimentions());

		//makes a little bit more easy versions for test
		player2->setSpeed(player2->getSpeed() - 1);
	}

	return success;
}



bool CGame::levelScreenLoop()
{
	bool success = true;


	if (!player2->loadMedia(gRenderer, mainFont))
	{
		printf("Failed to load rival's image!\n");
		success = false;
	}

	//creates an item
	std::unique_ptr <CThing> Apple = std::make_unique<CThing>("Apple");
	if (!Apple->loadMedia(gRenderer))
	{
		printf("Failed to load apple's image!\n");
		success = false;
	}


	//if all textures are loaded successfully
	if (success)
	{
		SDL_Event event;
		currentBackGroundClip = &backGroundSceneClips[levelScreen];

		SDL_Color textColor = { 0, 0, 0, 255 };//Set text color as black 

		//sets the starting positions
		player1->setPosition(0, (SCREEN_HEIGHT - player1->getHeight()) / 2);
		player2->setPosition(SCREEN_WIDTH - player2->getWidth(), (SCREEN_HEIGHT - player2->getHeight()) / 2);
		Apple->setPosition((SCREEN_WIDTH - Apple->getWidth()) / 2, (SCREEN_HEIGHT - Apple->getHeight()) / 2);

		pause = true; //pause enabled

		//creating pause texture
		std::unique_ptr< CTexture> pressEnterTexture = std::make_unique<CTexture>();
		if (!pressEnterTexture->loadFromRenderedText(gRenderer, "Press Enter to play", mainFont, textColor))
		{
			printf("Unable to render pause texture!\n");
		}

		while ((!quit) && (currentScene == levelScreen)) //While level screen is running 
		{
			quitCheck(event);
			if (pause) {
				draw(player1.get(), player2.get(), Apple.get(), NULL, pressEnterTexture.get());
			}

			if (!pause)
			{
				player1->PlayerLogic(Apple.get(), SCREEN_WIDTH, SCREEN_HEIGHT);
				player2->PlayerLogic(Apple.get(), SCREEN_WIDTH, SCREEN_HEIGHT);
				draw(player1.get(), player2.get(), Apple.get());

				//checks win condition
				if ((player1->getScore() >= victoryCondition) || (player2->getScore() >= victoryCondition))
				{
					if ((player1->getScore() >= victoryCondition) && (player2->getScore() >= victoryCondition))
					{
						winner = "Draw";
					}
					else if (player1->getScore() >= victoryCondition)
					{
						winner = "You won!";
					}
					else if (player2->getScore() >= victoryCondition)
					{
						winner = player2->getName() + " is winner!";
					}
					currentScene += 1;  //go to next screen
				};
			}
		}
	}

	return success;
}



bool CGame::finishScreenLoop()
{
	bool success = true;

	SDL_Event event;

	SDL_Color textColor = { 0, 0, 0 };

	//creates an inscription about victory
	std::unique_ptr<CTexture> winnerTexture = std::make_unique<CTexture>();
	if (!winnerTexture->loadFromRenderedText(gRenderer, winner, mainFont, textColor))
	{
		printf("Unable to render winnerTexture!\n");
	}

	draw(player1.get(), player2.get(), NULL, NULL, winnerTexture.get());

	//creates a button to repeat the game
	std::unique_ptr <CButton> repeatButton = std::make_unique <CButton>((SCREEN_WIDTH - 192) / 2, 400, 192, 72, 0, 120, 192, 72);
	repeatButton->DrawButton(gRenderer, buttonSpriteSheetTexture.get());

	//creates an exit from game button
	std::unique_ptr <CButton> exitButton = std::make_unique <CButton>((SCREEN_WIDTH - 192) / 2, 490, 192, 72, 0, 192, 192, 72);
	exitButton->DrawButton(gRenderer, buttonSpriteSheetTexture.get());

	//CButton captionButton = CButton(exitButton.getX()+ exitButton.getWidth()+20, exitButton.getY()-20, 107, 45, 0, 264, 107, 45);
	//captionButton.DrawButton(gRenderer, &buttonSpriteSheetTexture);

	//Update screen
	SDL_RenderPresent(gRenderer);


	while ((!quit) && (currentScene == finishScreen)) //While finish screen is running 
	{
		quitCheck(event);

		if (repeatButton->handleEvent(&event))
		{
			currentScene = selectorScreen;
		};

		if (exitButton->handleEvent(&event))
		{
			quit = true;
		};

		//if (captionButton.handleEvent(&event))
		//{
		//	currentScene = captionScreen;
		//};
	}
	player1->clear();
	player2->clear();

	return success;
}



void CGame::mainLoop()
{
	while (!quit) //While application is running 
	{
		mainLogic();
	}
}



void CGame::mainLogic()
{
	//switch between screens
	switch (currentScene)
	{
		case titleScreen:
		{
			titleScreenLoop();
			break;
		}
		case selectorScreen:
		{
			if (!characterSelectorScreenLoop())
			{
				printf("Fail in character selector image!\n");
				quit = true;
			}
			break;
		}
		case levelScreen:
		{
			if (!levelScreenLoop())
			{
				printf("Fail in level's images!\n");
				quit = true;
			}

			break;
		}
		case finishScreen:
		{
			if (!finishScreenLoop())
			{
				printf("Fail in finish!\n");
				quit = true;
			}
			break;
		}
		default:
		{
			printf("Something is going wrong!!! This scene does not exist!\n");
		}

	}
}



void CGame::titleAppears() {
	if (titleFrame < 300) {
		titleFrame += 3;
	}
	titleTexture->render(gRenderer, ((SCREEN_WIDTH - titleTexture->getWidth()) / 2), titleFrame - titleTexture->getHeight());
}



void CGame::draw(CPlayer* p1, CPlayer* p2, CThing* thing, CButton* button, CTexture* text)
{
	//Clear screen 
	SDL_RenderClear(gRenderer);

	//Render textures to screen 
	backGroundSheetTexture->render(gRenderer, 0, 0, currentBackGroundClip); //background texture

	//item texture
	if (thing != NULL) {
		thing->drawThing(gRenderer);
	}

	//player texture
	//drawing players depending on their position relative to each other
	if (p1 != NULL)
	{
		if (p2 != NULL) {
			if (p1->getFloorY() > p2->getFloorY())
			{
				p2->DrawPlayer(gRenderer, p2->getX(), p2->getY());
				p1->DrawPlayer(gRenderer, p1->getX(), p1->getY());
			}
			else
			{
				p1->DrawPlayer(gRenderer, p1->getX(), p1->getY());
				p2->DrawPlayer(gRenderer, p2->getX(), p2->getY());

			}
			p2->writeScore(gRenderer, (SCREEN_WIDTH / 2), 20, mainFont);
		}
		else
		{
			p1->DrawPlayer(gRenderer, p1->getX(), p1->getY());
		}
		p1->writeScore(gRenderer, 10, 20, mainFont);
	}

	if (p2 != NULL) {
		p2->writeScore(gRenderer, (SCREEN_WIDTH / 2), 20, mainFont);
	}

	//button texture
	if (button != NULL) {
		button->DrawButton(gRenderer, buttonSpriteSheetTexture.get());
	}

	if (currentScene == titleScreen) {
		titleAppears();
	}

	//if (currentScene != levelScreen && titleFrame>=300) {
	//	buttonCaption->Drawbutton(gRenderer, &buttonSpriteSheetTexture);
	//}

	//text texture
	if (text != NULL) {
		text->render(gRenderer, ((SCREEN_WIDTH - text->getWidth()) / 2), ((SCREEN_HEIGHT - text->getHeight()) / 2));
	}

	//Update screen
	SDL_RenderPresent(gRenderer);
}