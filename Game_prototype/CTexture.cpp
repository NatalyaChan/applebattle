#include "CTexture.h"


CTexture::CTexture()
{
	//Initialize 
	mTexture = NULL;
	textureSize = { 0,0 };
}



CTexture::~CTexture()
{
	//Deallocate 
	free();
}



int CTexture::getWidth()
{
	return textureSize.x;
}



int CTexture::getHeight()
{
	return textureSize.y;
}



bool CTexture::loadFromFile(SDL_Renderer* ren, std::string path)
{
	free();  //Get rid of preexisting texture 

	SDL_Texture* newTexture = NULL;  //The final texture 

	//Load image at specified path 
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));  //Color key image 

		newTexture = SDL_CreateTextureFromSurface(ren, loadedSurface);  //Create texture from surface pixels 
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
		else
		{
			textureSize = { loadedSurface->w,loadedSurface->h };  //Get image dimensions 
		}

		SDL_FreeSurface(loadedSurface);  //Get rid of old loaded surface 
	}

	mTexture = newTexture;

	return mTexture != NULL;  //Return success
}



bool CTexture::loadFromRenderedText(SDL_Renderer* ren, std::string textureText, TTF_Font* font, SDL_Color textColor)
{
	free();  //Get rid of preexisting texture 

	SDL_Surface* textSurface = TTF_RenderText_Solid(font, textureText.c_str(), textColor);
	if (textSurface == NULL) {
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	}
	else
	{
		mTexture = SDL_CreateTextureFromSurface(ren, textSurface);  //Create texture from surface pixels 
		if (mTexture == NULL)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			textureSize = { textSurface->w, textSurface->h };  //Get image dimensions 
		}

		SDL_FreeSurface(textSurface);  //Get rid of old surface 
	}

	return mTexture != NULL;  //Return success 
}



void CTexture::render(SDL_Renderer* ren, int x, int y, SDL_Rect* clip)
{
	SDL_Rect renderQuad = { x, y, getWidth(), getHeight() };  //Set rendering space

	//Set clip rendering dimensions 
	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy(ren, mTexture, clip, &renderQuad);  //render to screen
}



void CTexture::render(SDL_Renderer* ren, int x, int y, int w, int h, SDL_Rect* clip)
{

	SDL_Rect renderQuad = { x, y, w, h }; //Set rendering space
	SDL_RenderCopy(ren, mTexture, clip, &renderQuad);  //render to screen
}



void CTexture::free()
{
	//Free texture if it exists 
	if (mTexture != NULL)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		textureSize = { 0,0 };
	}
}