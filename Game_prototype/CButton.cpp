#include "CButton.h"



CButton::CButton()
{
}



CButton::CButton(int x, int y, int w, int h, int spriteX, int spriteY, int spriteW, int spriteH)
{
	buttonPosition = { x,y };
	buttonSize = { w, h };
	setButtonSpriteClip(spriteX, spriteY, spriteW, spriteH);
}



CButton::~CButton()
{
	buttonText->free();
}



void CButton::setPosition(int x, int y)
{
	buttonPosition = { x,y };
}



int CButton::getX()
{
	return buttonPosition.x;
}



int CButton::getY()
{
	return buttonPosition.y;
}



void CButton::setSize(int w, int h)
{
	buttonSize = { w, h };
}



int CButton::getWidth()
{
	return buttonSize.x;
}



int CButton::getHeight()
{
	return buttonSize.y;
}



void CButton::setButtonSpriteClip(int x, int y, int w, int h)
{
	spriteButtonClip = { x,y,w,h };
}



bool CButton::handleEvent(SDL_Event* e)
{
	bool press = false;

	//If mouse event happened 
	if (e->type == SDL_MOUSEBUTTONDOWN)
	{
		//Get mouse position 
		int x, y;
		SDL_GetMouseState(&x, &y);

		if ((x >= getX()) &&
			(x <= getX() + getWidth()) &&
			(y >= getY()) &&
			(y <= getY() + getHeight()))
		{
			press = true;
		}
	}
	return press;
}



void CButton::addTextOnButton(SDL_Renderer* ren, std::string title, TTF_Font* font, SDL_Color textColor, int align)   // align =0= top; 1=middle; 2=bottom
{
	if (!buttonText->loadFromRenderedText(ren, title, font, textColor))
	{
		printf("Unable to render buttonText texture!\n");
	}
	else
	{
		buttonTextPosition = { (buttonPosition.x + (buttonSize.x - buttonText->getWidth()) / 2),
							(buttonPosition.y + (buttonSize.y - buttonText->getHeight() - 10) * align / 2 + 5) };
	}
	buttonText->render(ren, buttonTextPosition.x, buttonTextPosition.y);
}



void CButton::DrawButton(SDL_Renderer* ren, CTexture* spriteSheet)
{
	//Show current button sprite 
	if (spriteSheet != NULL)
	{
		spriteSheet->render(ren, buttonPosition.x, buttonPosition.y, &spriteButtonClip);
	}

	if (buttonText != NULL)
	{
		buttonText->render(ren, buttonTextPosition.x, buttonTextPosition.y);
	}
}