#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "CThing.h"
#include "CTexture.h"
#include <vector>

#ifndef CPLAYER_H
#define	CPLAYER_H

class CPlayer {
public:
	CPlayer();
	CPlayer(std::string newname, std::string sizeCategory, int speed = 3, std::vector<SDL_Point> newSpriteDimentions = { {77,61},{77,61},{41,52},{41,58},{62,55} });
	virtual ~CPlayer();

	void setMainAttributes(std::string newname, int speed, std::vector<SDL_Point> newSpriteDimentions);
	void setName(std::string newname);
	std::string getName();

	void setPosition(int x, int y);
	int getX();
	int getY();
	int getFloorY();

	int getWidth();
	int getHeight();

	void setsizeCategory(std::string sizeCategory);
	std::string getsizeCategory();

	void setSpeed(int speed);
	int getSpeed();

	int getScore();
	void clearScore();

	void setSpriteDimentions(std::vector<SDL_Point> newSpriteDimentions);
	std::vector<SDL_Point> getSpriteDimentions();

	void moveUp();
	void moveDown();
	void moveLeft();
	void moveRight();

	bool isFruitUp(CThing* t);
	bool isFruitDown(CThing* t);
	bool isFruitLeft(CThing* t);
	bool isFruitRight(CThing* t);
	void CheckFruit(CThing* t, int screenwidth, int screenheight);

	bool loadMedia(SDL_Renderer* ren, TTF_Font* font);  //loading player texture
	virtual void PlayerLogic(CThing* t, int screenwidth, int screenheight);
	void writeScore(SDL_Renderer* ren, int x, int y, TTF_Font* font);
	void DrawPlayer(SDL_Renderer* ren, int x, int y);

	void clear();
private:
	std::string name = "Default";
	SDL_Point playerPosition = { 200,400 };
	std::string sizeCategory = "Medium";
	int speed = 3;
	int score = 0;

	int nFrames = 4;  //number of frames in animation
	int currentFrame = 0;
	std::vector<SDL_Point> spriteDimentions = { {0,0},{0,0},{0,0},{0,0},{0,0} };

	std::unique_ptr <CTexture> spriteSheetTexture = std::make_unique<CTexture>();
	//The images that correspond to a keypress
	std::vector<SDL_Rect> spriteLeftClips;
	std::vector<SDL_Rect> spriteRightClips;
	std::vector<SDL_Rect> spriteUpClips;
	std::vector<SDL_Rect> spriteDownClips;
	SDL_Rect spriteIdleClip;
	SDL_Rect* currentClip;

	std::unique_ptr <CTexture>  promptScoreTexture = std::make_unique<CTexture>();
	std::unique_ptr <CTexture> scoreTexture = std::make_unique<CTexture>();
};


#endif //CPLAYER_H