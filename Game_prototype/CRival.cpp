#include "CRival.h"



CRival::CRival()
{
}



CRival::~CRival()
{
}



void CRival::PlayerLogic(CThing* t, int screenwidth, int screenheight)
{
	//checks where the fruit is and goes to it.
	if (isFruitUp(t))
	{
		moveUp();
	}

	else if (isFruitDown(t))
	{
		moveDown();
	}

	else if (isFruitLeft(t))
	{
		moveLeft();
	}

	else if (isFruitRight(t))
	{
		moveRight();
	}

	CheckFruit(t, screenwidth, screenheight);
}
